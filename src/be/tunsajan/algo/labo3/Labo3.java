/**
 * ALGO: TEST_PROFILER
 *
 * Conclusion apres les differents test du profiler j en deduis que
 * si l element se trouve dans le tableau la recherche dicchotomique est
 * plus efficace par contre si l element recherche se trouve au extremité 
 * la recherche sequencielle est plus efficace
 * 
 * @author TUNSAJAN Somboom
 */
package be.tunsajan.algo.labo3;

public class Labo3 {

    final public static int MIN_VALUE = -999999999;
    final public static int MAX_VALUE =  999999999;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int tableauTrie[] = new int[1000];
        generer(tableauTrie);
        rechercheDichotomique(tableauTrie, tableauTrie[999], 0, 999);
        rechercheSeq(tableauTrie, tableauTrie[999]);

    }

    private static void generer(int tabInteger[]) {
        for (int i = 0; i < tabInteger.length; i++) {
            tabInteger[i] = (int) (MIN_VALUE+ (Math.random() * (MAX_VALUE - MIN_VALUE)));
        }
        triInsertion(tabInteger);
        /* J ai fait comme ca pou etre sur d avoir un grand panel de nombre different */

    }
    private static void inserer(int element, int tab[], int tailleGauche) {
        int j;
        for (j = tailleGauche; j > 0 && tab[j - 1] > element; j--) {
            tab[j] = tab[j - 1];
        }
        tab[j] = element;
    }

    private static void triInsertion(int tab[]) {
        int i;
        for (i = 1; i < tab.length; ++i) {
            inserer(tab[i], tab, i);
        }
    }

    private static void afficherTableau(int tab[]) {
        for (int i = 0; i < tab.length; i++) {
            System.out.print("\t" + tab[i]);
        }
    }

    private static int rechercheDichotomique(int tableauTrié[], int valeurRecherchée, int borneInf, int borneSup) {
        int indiceMilieu;
        int indice;
        if (borneInf > borneSup) {
            indice = -1;
        } else {
            indiceMilieu = borneInf + ((borneSup - borneInf) / 2);
            if (tableauTrié[indiceMilieu] == valeurRecherchée) {
                indice = indiceMilieu;
            } else if (tableauTrié[indiceMilieu] < valeurRecherchée) {
                indice = rechercheDichotomique(tableauTrié, valeurRecherchée, indiceMilieu + 1, borneSup);
            } else {
                indice = rechercheDichotomique(tableauTrié, valeurRecherchée, borneInf, indiceMilieu - 1);
            }
        }
        return indice;
    }
    private static int rechercheSeq(int tab[], int valeurRecherche){
        for(int i = 0; i<tab.length;i++)
            if(tab[i] == valeurRecherche) return i;
        return -1;
    }
    
}
